#include <iostream>
#include "triangulo.hpp"

triangulo::triangulo() {
	setBase(5);
	setAltura(15);
}
triangulo::triangulo(float base, float altura) {
	setBase(base);
	setAltura(altura);
}
float triangulo::area(){
	return ( ( getBase() * getAltura() )/2 );
}
