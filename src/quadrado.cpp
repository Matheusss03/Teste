#include <iostream>
#include "quadrado.hpp"

using namespace std;

quadrado::quadrado(){
	setBase(10);
	setAltura(10);
}

quadrado::quadrado(float base) {
	setBase(base);
	setAltura(base);
}
float quadrado::area(){
	return getBase() * getBase();
}
