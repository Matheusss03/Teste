#include <iostream>
#include "geometrica.hpp"
#include "triangulo.hpp"
#include "quadrado.hpp"
#include "circulo.hpp"

using namespace std;

int main(int argc, char ** argv) {
	geometrica * FormaGeometrica1 = new geometrica();
	geometrica FormaGeometrica2(10,20);
	geometrica FormaGeometrica3 = geometrica(40,50);

	triangulo * Triangulo1 = new triangulo();
	quadrado * Quadrado1 = new quadrado();
	circulo * Circulo1 = new circulo();

	cout << "Area FormaGeometrica1 = " << FormaGeometrica1->area() << endl;
	cout << "Area FormaGeometrica2 = " << FormaGeometrica2.area() << endl;
	cout << "Area FormaGeometrica3 = " << FormaGeometrica3.area() << endl;

	cout << "Area Triangulo1 = " << Triangulo1 -> area() << endl;

	cout << "Area Quadrado1 = " << Quadrado1 -> area() << endl;

	cout << "Area Circulo1 = " << Circulo1 -> area() << endl;

}

	geometrica *lista_de_formas[10] = {FormaGeometrica1, Triangulo1, Quadrado1, Circulo1};

	for (int i=0; i < 4; i++){
		cout << "Area" << i << ":" << lista_de_formas[i]-> area() << endl;
}
}
