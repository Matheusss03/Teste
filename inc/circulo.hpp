#ifndef CIRCULO_H
#define CIRCULO_H

#include "geometrica.hpp"

class circulo : public geometrica{
        public:
                circulo();
                circulo(float base);

                float area();
};

#endif 
